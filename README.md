# Almundo examen backend

En este repositorio se encuentra el código y la documentación de examen de backend para almundo.

## Instrucciones para ejecutar el aplicativo 

* Para ejecutar el programa es necesario utilizar java 1.8 o superior
* Abrir la consola y ubicarse dentro de la carpeta code
* Una vez dentro, ejecutar el siguiente comando en consola

```
java -jar callcenter.jar
```

## Consideraciones

* Para resolver los puntos extras se manejó un condicional en el que se examina si el número de llamadas que están siendo procesadas es menor a diez y si hay empleados disponibles, en el caso de ser falsa la afirmación, La llamada conserva su turno en la cola de llamadas y se retorna un mensaje indicando que se debe esperar mientras un empleado se encuentra disponible.

* Respecto a los test unitarios, me falta conocimientos acerca de la ejecución de test unitarios en aplicaciones concurrentes, así que lo que hice en este aplicativo fue imprimir en pantalla el proceso de atender 11 llamadas concurrentemente.

## Diagramas UML

Para explicar el código implementado utilicé tres diagramas:

* Diagrama de clases: para entender como se estructuró la solución al problema.

* Diagrama de casos de uso: para desglosar que casos intervienen en la solución del problema.

* Maquina de estados de llamada: Para entender que disparadores y que condiciones son necesarias para que una llamada complete su ciclo de atención.

## Estructura del proyecto

* code/callcenter/src: Código fuente en Java
* code/callcenter/target: Código compilado en Java
* code/callcenter.jar: Archvio ejecutable 
* docs: Diagramas UML en formato PNG y archivo EAP (Enterprise Architect)
