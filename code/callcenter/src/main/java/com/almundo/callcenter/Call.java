package com.almundo.callcenter;

public class Call {

	private int id; //identificador de la llamada
	private int duration; //tiempo que tarda una llamada en ser atendida
	
	public Call(int id, int duration){
        this.id = id;
        this.duration = duration;
    }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	
}
