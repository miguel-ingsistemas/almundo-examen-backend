package com.almundo.callcenter;

public interface Employee {

	//Firma creda con el fin de empezar una llamada
	public void beginCall(Call call);
	
	//Firma que permite procesar una llamada
	public void processCall();
	
	//Firma que permite identificar si un empleado esta disponible
	public boolean isAvailable();
}
