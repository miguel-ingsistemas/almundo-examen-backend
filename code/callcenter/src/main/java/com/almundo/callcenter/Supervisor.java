package com.almundo.callcenter;

public class Supervisor implements Employee{
	
	private int id;
	private boolean available;
	private Call call;
	
	public Supervisor(int id){
		this.id = id;
		available = true;
	}
	
	public void beginCall(Call call){
		this.call = call;
		System.out.println("Supervisor "+id+" answering call "+call.getId());
		available = false;
	}
	
	public void processCall(){
		if(call.getDuration() == 0){
			System.out.println("Supervisor "+id+" finishing call "+call.getId());
			available = true;			
		}else{
			call.setDuration(call.getDuration() - 1);
		}
	}
	
	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
}
