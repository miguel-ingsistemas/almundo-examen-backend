package com.almundo.callcenter;

import java.util.ArrayList;
import java.util.Iterator;

public class EmployeeFactory {
	private ArrayList<Employee> employees;
	private int busyEmployees;
	
	//constructor donde se asigna a la lista de empleados los tipos en orden de atención de llamadas
	public EmployeeFactory(int operators, int supervisors, int directors){
		employees = new ArrayList<Employee>();
		busyEmployees = 0;
		
		for (int i = 1; i <= operators; i++) {
			employees.add(new Operator(i));
		}
		for (int i = 1; i <= supervisors; i++) {
			employees.add(new Supervisor(i));
		}
		for (int i = 1; i <= directors; i++) {
			employees.add(new Director(i));
		}
	}

	//obtiene un empleado de la lista de empleados en el caso de estar disponible, si no se encuentra nadie disponible retorna null
	public Employee getEmployee() {
		Iterator<Employee> employeeIterator = employees.iterator();
		Employee selectedEmployee = null;
		while(employeeIterator.hasNext() && selectedEmployee == null){
			Employee employee = employeeIterator.next();
			if(employee.isAvailable()){
				selectedEmployee = employee;
			}
		}		
		return selectedEmployee;
	}
	
	//Itera sobre la lista de empleados y procesa la llamada en caso de que el empleado este ocupado
	public void processCalls(){
		busyEmployees = 0;
		Iterator<Employee> employeeIterator = employees.iterator();
		
		while(employeeIterator.hasNext()){
			Employee employee = employeeIterator.next();
			if(!employee.isAvailable()){
				employee.processCall();
				busyEmployees++;
			}
		}
	}
	
	//retorna el número de empleados ocupados
	public int getBusyEmployees() {
		return busyEmployees;
	}
	
	//agrega empleados que se encuentran atendiendo una llamada
	public void addBusyEmployee(){
		busyEmployees++;
	}
}
