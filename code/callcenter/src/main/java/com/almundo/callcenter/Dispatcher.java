package com.almundo.callcenter;

import java.util.ArrayList;
import java.util.Iterator;

public class Dispatcher implements Runnable {
	
	private ArrayList<Call> calls; //cola de llamadas
	private EmployeeFactory employeeFactory; //factory de empleados
	private int maxEmployees; //Número máximo de llamas que pueden ser procesadas
	
	public Dispatcher(EmployeeFactory employeeFactory){
		calls = new ArrayList<Call>();
		this.employeeFactory = employeeFactory;
		maxEmployees = 10;
	}
	
	//Añadir nueva llamada a la cola de llamadas
	public void dispatchCall(Call call) {
		calls.add(call);
	}
	
	//Hilo encargado de procesar las llamadas
    public void run() {
    	Iterator<Call> callIterator = calls.iterator(); //iterador de la cola de llamadas
    	
    	//mientras existan llamadas pendientes por atender o algún empleado se encuentre ocupado, seguir con el ciclo de atender llamadas
    	while (callIterator.hasNext() || employeeFactory.getBusyEmployees() > 0){
            try {
            	//método encargado de procesar las llamadas que ya han sido asignadas
            	employeeFactory.processCalls();
            	
            	//mientras existan llamadas pendientes por atender y el número de empleados ocupados sea menor al máximo permitido, asignar llamada
            	while(callIterator.hasNext() && employeeFactory.getBusyEmployees() < maxEmployees){
            		Employee employee = employeeFactory.getEmployee();
            		//si hay un empelado disponible, comenzar llamada, agregar empleado ocupado a la factory y remover llamada de la cola de llamadas
            		if(employee != null){
                		employee.beginCall(callIterator.next());
            			employeeFactory.addBusyEmployee();
                		callIterator.remove();            			
            		}
            	}
            	
            	//Si hay llamadas en la cola de espera y todos los empleados estan ocupados entonces mensaje
            	if(callIterator.hasNext() && employeeFactory.getBusyEmployees() == maxEmployees){
        			System.out.println("Por favor espere, estamos atendiendo el número máximo de llamadas");
        		}
            	Thread.sleep(0);
            } catch (InterruptedException e) {
                System.out.println(e.toString());
            }
        }
    	
    	System.out.println("Se han atendido todas las llamadas!");
    }

}
