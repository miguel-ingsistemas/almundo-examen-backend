package com.almundo.callcenter;

public class App 
{
    public static void main( String[] args )
    {
    	//Crear  factory de empleados con 6 operarios, 3 supervisores y 1 director
    	EmployeeFactory employeeFactory = new EmployeeFactory(6, 3, 1);
    	
    	//Crear dispatcher y asignarle la factory que hemos creado
    	Dispatcher d = new Dispatcher(employeeFactory);
    	
    	//Crear llamadas con id de llamada y segundos que toma en ser procesada
    	d.dispatchCall(new Call(1, 8));
    	d.dispatchCall(new Call(2, 5));
    	d.dispatchCall(new Call(3, 7));
    	d.dispatchCall(new Call(4, 6));
    	d.dispatchCall(new Call(5, 5));
    	d.dispatchCall(new Call(6, 8));
    	d.dispatchCall(new Call(7, 10));
    	d.dispatchCall(new Call(8, 5));
    	d.dispatchCall(new Call(9, 5));
    	d.dispatchCall(new Call(10, 7));
    	d.dispatchCall(new Call(11, 7));

    	//Crear y arrancar hilo
    	Thread thread = new Thread(d);
        thread.start();
    }
}
